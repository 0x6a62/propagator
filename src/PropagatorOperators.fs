namespace Propagator

/// Built-in operators that can be applied using propagators
module PropagatorOperators =
  open Propagator.Internal
  open Propagator.Internal.Types
  open Propagator.Types

  ///////////////////////////////////////////
  // Operators applied to the datatype values
  // (used by the propagator operator wrappers)

  /// Addition for two propagator cell values (op1 + op2)
  let propagatorValueAdd (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (I(op1'), I(op2')) -> Some(I(op1' + op2'))
    | (F(op1'), F(op2')) -> Some(F(op1' + op2'))
    | (M(op1'), M(op2')) -> Some(M(op1' + op2'))
    | (B(_), B(_)) -> raise (NotSupportedException("Addition of Boolean"))
    | (V(op1'), V(op2')) -> Some(V(op1' + op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None // Invalid

  /// Subtraction for two propagator cell values (op1 - op2)
  let propagatorValueSubtract (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (I(op1'), I(op2')) -> Some (I(op1' - op2'))
    | (F(op1'), F(op2')) -> Some (F(op1' - op2'))
    | (M(op1'), M(op2')) -> Some (M(op1' - op2'))
    | (B(_), B(_)) -> raise (NotSupportedException("Subtraction of Boolean"))
    | (V(op1'), V(op2')) -> Some(V(op1' - op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _, _ -> None // Invalid

  /// Multiplication for two propagator cell values (op1 * op2)
  let propagatorValueMultiply (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (I(op1'), I(op2')) -> Some (I(op1' * op2'))
    | (F(op1'), F(op2')) -> Some (F(op1' * op2'))
    | (M(op1'), M(op2')) -> Some (M(op1' * op2'))
    | (B(_), B(_)) -> raise (NotSupportedException("Multiplication of Boolean"))
    | (V(op1'), V(op2')) -> Some(V(op1' * op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None //Invalid

  /// Divison for two propagator cell values (op1 / op2)
  let propagatorValueDivide (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (I(op1'), I(op2')) -> if op2' <> 0 && op1' % op2' = 0 then Some (I(op1' / op2')) else None // Invalid 
    | (F(op1'), F(op2')) -> if op2' <> 0. then Some (F(op1' / op2')) else None // Invalid 
    | (M(op1'), M(op2')) -> if op2' <> 0m then Some (M(op1' / op2')) else None // Invalid 
    | (B(_), B(_)) -> raise (NotSupportedException("Division of Boolean"))
    | (V(op1'), V(op2')) -> Some(V(op1' / op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None //Invalid

  /// Pow for two propagator cell values (op1 ** op2)
  let propagatorValuePow (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (I(op1'), I(op2')) -> Some (I(int (System.Math.Pow(double op1', double op2'))))
    | (F(op1'), F(op2')) -> Some (F(float (System.Math.Pow(op1', op2'))))
    | (M(_), M(_)) -> raise (NotSupportedException("Pow of Decimal"))
    | (B(_), B(_)) -> raise (NotSupportedException("Pow of Boolean"))
    | (V(op1'), V(op2')) -> Some(V(Interval.Pow op1' op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None //Invalid

  /// Root for two propagator cell values (op1 ** 1/op2)
  let propagatorValueRoot (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (I(op1'), I(op2')) -> Some (I(int (System.Math.Pow(double op1', double 1 / double op2'))))
    | (F(op1'), F(op2')) -> Some (F(float (System.Math.Pow(op1', 1. / op2'))))
    | (M(_), M(_)) -> raise (NotSupportedException("Root of Decimal"))
    | (B(_), B(_)) -> raise (NotSupportedException("Root of Boolean"))
    | (V(op1'), V(op2')) -> Some(V(Interval.Root op1' op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None //Invalid

  /// Log for two propagator cell values (log_op2 op1)
  let propagatorValueLog (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (I(op1'), I(op2')) -> Some (I(int (System.Math.Log(double op1', double op2'))))
    | (F(op1'), F(op2')) -> Some (F(System.Math.Log(op1', op2')))
    | (M(_), M(_)) -> raise (NotSupportedException("Log of Decimal"))
    | (B(_), B(_)) -> raise (NotSupportedException("Log of Boolean"))
    | (V(op1'), V(op2')) -> Some(V(Interval.Root op1' op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None //Invalid

  /// Logical And for two propagator cell values (op1 & op2)
  let propagatorValueAnd (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (B(op1'), B(op2')) -> Some (B(op1' && op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None // Invalid

  /// Logical And-alternate for two propagator cell values (x & op1 = op2)
  let propagatorValueAnd2 (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (B(op1'), B(op2')) -> if op2' then Some(B(true))
                            else if op1' then Some(B(false))
                            else None // Not enough info
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None //Some Invalid

  /// Logical Or for two propagator cell values (op1 | op2)
  let propagatorValueOr (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (B(op1'), B(op2')) -> Some(B(op1' || op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None //Invalid

  /// Logical Or-alternate for two propagator cell values (x | op1 = op2)
  let propagatorValueOr2 (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (B(op1'), B(op2')) -> if op1' && op2' then None 
                            else if not op1' && op2' then Some(B(true))
                            else if not op1' && not op2' then None // Some(B(false))
                            else None 
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None //Some Invalid
  
  /// Logical xor for two propagator cell values (op1 ^ op2)
  let propagatorValueXor (op1: DataType) (op2: DataType) :DataType option =
    match (op1, op2) with
    | (B(op1'), B(op2')) -> Some (B(op1' <> op2'))
    | Contradiction, _ -> Some Contradiction
    | _, Contradiction -> Some Contradiction
    | _,_ -> None // Invalid

  ///////////////////////
  // Propagator syntax #1

  let adder (op1: Cell) (op2: Cell) (op3: Cell) =
    let adder = { 
        PropagatorOperator.f1 = propagatorValueAdd;
        f2 = flip propagatorValueSubtract;
        f3 = flip propagatorValueSubtract; }

    PropagatorNetwork.CreatePropagator adder op1 op2 op3

  /// Propagator operator: -
  let subtracter (op1: Cell) (op2: Cell) (op3: Cell) = 
    let subtracter = { 
        PropagatorOperator.f1 = propagatorValueSubtract;
        f2 = propagatorValueSubtract;
        f3 = propagatorValueAdd; }

    PropagatorNetwork.CreatePropagator subtracter op1 op2 op3

  /// Propagator operator: *
  let multiplier (op1: Cell) (op2: Cell) (op3: Cell) = 
    let multiplier = { 
        PropagatorOperator.f1 = propagatorValueMultiply;
        f2 = flip propagatorValueDivide;
        f3 = flip propagatorValueDivide; }

    PropagatorNetwork.CreatePropagator multiplier op1 op2 op3

  /// Propagator operator: /
  let divider (op1: Cell) (op2: Cell) (op3: Cell) = 
    let multiplier = { 
        PropagatorOperator.f1 = propagatorValueDivide;
        f2 = propagatorValueDivide;
        f3 = propagatorValueMultiply; }

    PropagatorNetwork.CreatePropagator multiplier op1 op2 op3

  let power (op1: Cell) (op2: Cell) (op3: Cell) =
    let ten = DataType.F 10.

    let powerOp = { 
        PropagatorOperator.f1 = propagatorValuePow;
        f2 = (fun (a: DataType) (c: DataType) -> 
          let num = propagatorValueLog c ten
          let den = propagatorValueLog a ten
          printfn "num: %A den: %A" num den
          if num.IsSome && den.IsSome then propagatorValueDivide num.Value den.Value
          else None);
        f3 = flip propagatorValueRoot; }

    PropagatorNetwork.CreatePropagator powerOp op1 op2 op3
  

  /// Propagator operator: & (boolean and)
  let ander (op1: Cell) (op2: Cell) (op3: Cell) = 
    let ander = { 
        PropagatorOperator.f1 = propagatorValueAnd;
        f2 = propagatorValueAnd2;
        f3 = propagatorValueAnd2; }

    PropagatorNetwork.CreatePropagator ander op1 op2 op3

  /// Propagator operator: | (boolean or)
  let orer (op1: Cell) (op2: Cell) (op3: Cell) = 
    let orer = { 
        PropagatorOperator.f1 = propagatorValueOr;
        f2 = propagatorValueOr2;
        f3 = propagatorValueOr2; }

    PropagatorNetwork.CreatePropagator orer op1 op2 op3

  /// Propagator operator: ^ (boolean xor)
  let xorer (op1: Cell) (op2: Cell) (op3: Cell) = 
    let xorer = { 
        PropagatorOperator.f1 = propagatorValueXor;
        f2 = propagatorValueXor;
        f3 = propagatorValueXor; }

    PropagatorNetwork.CreatePropagator xorer op1 op2 op3

  ///////////////////////
  // Propagator syntax #2 
  // (alternate syntax)
  // operators are partial applications, equals  applies op3 to the operator partial applicaiton

  /// Propagator: = (syntax sugar) 
  let (^=) (op3: Cell) f :Propagator = f op3
  
  /// Propagator: <- (equals, but more f#-ish) (syntax sugar) 
  let (^<-) (op3: Cell) f :Propagator = f op3

  /// Propagator operator: +
  let (^+) (op1: Cell) (op2: Cell) =
    let adder = { 
        PropagatorOperator.f1 = propagatorValueAdd;
        f2 = flip propagatorValueSubtract;
        f3 = flip propagatorValueSubtract; }

    PropagatorNetwork.CreatePropagator adder op1 op2

  /// Propagator operator: -
  let (^-) (op1: Cell) (op2: Cell) = 
    let subtracter = { 
        PropagatorOperator.f1 = propagatorValueSubtract;
        f2 = propagatorValueSubtract;
        f3 = propagatorValueAdd; }

    PropagatorNetwork.CreatePropagator subtracter op1 op2

  /// Propagator operator: *
  let (^*) (op1: Cell) (op2: Cell) = 
    let multiplier = { 
        PropagatorOperator.f1 = propagatorValueMultiply;
        f2 = flip propagatorValueDivide;
        f3 = flip propagatorValueDivide; }

    PropagatorNetwork.CreatePropagator multiplier op1 op2

  /// Propagator operator: /
  let (^/) (op1: Cell) (op2: Cell) = 
    let multiplier = { 
        PropagatorOperator.f1 = propagatorValueDivide;
        f2 = propagatorValueDivide;
        f3 = propagatorValueMultiply; }

    PropagatorNetwork.CreatePropagator multiplier op1 op2
  
  /// Propagator operator: **
  let (^**) (op1: Cell) (op2: Cell) =
    let ten = DataType.F 10.

    let power = { 
        PropagatorOperator.f1 = propagatorValuePow;
        f2 = (fun (a: DataType) (c: DataType) -> 
          let num = propagatorValueLog c ten
          let den = propagatorValueLog a ten
          printfn "num: %A den: %A" num den
          if num.IsSome && den.IsSome then propagatorValueDivide num.Value den.Value
          else None);
        f3 = flip propagatorValueRoot; }

    PropagatorNetwork.CreatePropagator power op1 op2
  
  
  /// Propagator operator: & (boolean and)
  let (^&) (op1: Cell) (op2: Cell) = 
    let ander = { 
        PropagatorOperator.f1 = propagatorValueAnd;
        f2 = propagatorValueAnd2;
        f3 = propagatorValueAnd2; }

    PropagatorNetwork.CreatePropagator ander op1 op2

  /// Propagator operator: | (boolean or)
  let (^|) (op1: Cell) (op2: Cell) = 
    let orer = { 
        PropagatorOperator.f1 = propagatorValueOr;
        f2 = propagatorValueOr2;
        f3 = propagatorValueOr2; }

    PropagatorNetwork.CreatePropagator orer op1 op2

  /// Propagator operator: ^ (boolean xor)
  let (^^) (op1: Cell) (op2: Cell) = 
    let xorer = { 
        PropagatorOperator.f1 = propagatorValueXor;
        f2 = propagatorValueXor;
        f3 = propagatorValueXor; }

    PropagatorNetwork.CreatePropagator xorer op1 op2


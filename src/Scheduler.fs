namespace Propagator

open System
open Propagator.Internal

/// Job Id (internal)
type JobId = int

/// Scheduler Job
[<Struct>]
type Job = {
  /// Job Id (internal)
  Id: JobId
  /// Timestamp
  Timestamp: DateTime
  /// Function to execute
  JobFunction: int -> bool } with

  /// Create new Job Id
  static member NewJobId(): JobId =
    rand.Next(Int32.MaxValue)

/// Scheduler for propagator execution
type Scheduler() =
  let mutable queue: Job list = []
  let mutable queueEnd: Job list = []

  /// Add job to scheduler queue
  member __.EnQueue(job: Job) = queueEnd <- job :: queueEnd

  /// Clear scheduler queue
  member __.Clear() =
    queue <- []
    queueEnd <- []

  /// Length of scheduler queue
  member __.Length() = queue.Length + queueEnd.Length

  /// Is the scheduler queue empty
  member __.IsEmpty() = List.isEmpty queue && List.isEmpty queueEnd

  /// Execute scheduler job
  member __.ExecJob(job: Job): bool =
    // printfn "ExecJob: %i %A" job.Id job.JobFunction
    let result = job.JobFunction job.Id
    // printfn "ExecJob.result: %b" result
    result

  /// Run queued jobs
  member __.Run() =
    // printfn "Scheduler.Run... (length: %d)" (__.Length())
    while not (__.IsEmpty()) do
      match queue with
      | job :: newQueue ->
        __.ExecJob job |> ignore
        queue <- newQueue
      | [] ->
        if List.isEmpty queueEnd then
          ()
        else
          queue <- List.rev queueEnd
          queueEnd <- []

  /// Dump scheduler queue (for debugging)
  member __.DumpQueue(title: string) =
    printfn "Scheduler Queue: %s" title
    [ queue
      List.rev queueEnd ]
    |> List.concat
    |> List.iter (fun x -> printfn "%A" x)

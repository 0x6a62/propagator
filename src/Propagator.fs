namespace Propagator

open System
open System.Collections.Concurrent
open Propagator.Internal
open Propagator.Internal.Types
open Propagator.Types

/// Propagator network
type PropagatorNetwork() =
    let cells = ConcurrentDictionary<CellId, CellDetail>()
    let mutable propagators: Propagator list = []
    let scheduler = Scheduler()

    // Functions
    let emptyCell =
        { Id = 0
          Value = None
          IsConstant = false }

    static member private Log(s: string) =
        // Currently NOP
        // printfn "%s" s
        ()

    /// Are two cell values equal enough? (within epsilon)
    static member private Equalish (a: DataType option) (b: DataType option) =
        match (a, b) with
        | Some(I(a')), Some(I(b')) -> a' = b'
        | Some(F(a')), Some(F(b')) -> EqualishFloat a' b'
        | Some(M(a')), Some(M(b')) -> EqualishDecimal a' b'
        | Some(B(a')), Some(B(b')) -> a' = b'
        | Some(V(a')), Some(V(b')) -> a' = b'
        | _ -> false

    /// Convert a value to a supported datatype
    static member private ToDataType(v: Object) =
        match v with
        | :? int -> I(v :?> Int32)
        | :? double -> F(v :?> float)
        | :? decimal -> M(v :?> decimal)
        | :? bool -> B(v :?> bool)
        | :? Interval -> V(v :?> Interval)
        | _ -> raise (DataTypeException("Invalid propagator data type"))

    /// Create a cell value
    static member private CreateCellValue(v: 'a option) =
        if v.IsSome then Some(PropagatorNetwork.ToDataType v.Value) else None

    /// Are all items in the list identical
    static member private IsListAllIdentical(a: 'a list) =
        if List.isEmpty a
        then true
        else not (List.exists (fun x -> x <> List.head a) a)

    /// Create a cell
    static member private CreateCell(v: CellValue) =
        let cellId = CellDetail.NewCellId()
        { Id = cellId
          Value = v
          IsConstant = false }

    /// Create a constant-value cell
    static member private CreateConstantCell(v: CellValue) =
        let cellId = CellDetail.NewCellId()
        { Id = cellId
          Value = v
          IsConstant = true }

    /// Merge two cells together (typically as a part of an update process)
    static member private MergeCells (oldCell: CellDetail) (newCell: CellDetail) =
        // printfn "merging.. %A and %A" oldCell newCell

        let mergeCells' oldValue newValue =
            // If the values are intervals, we can merge them.
            // Other datatypes cannot be merged (different = contradiction)
            match (oldValue, newValue) with
            | V(oldValue'), V(newValue') ->
                let mergedInterval = Interval.Intersect oldValue' newValue'
                if mergedInterval.IsSome then
                    if PropagatorNetwork.Equalish (Some oldValue) (Some(V(mergedInterval.Value)))
                    then (false, oldCell)
                    else (true, { newCell with Value = Some(V(mergedInterval.Value)) })
                else
                    //raise (InvalidIntervalException("Intervals don't intersect", oldValue, newValue))
                    printfn "LOG ERROR: %s %A %A" "Intervals don't intersect" oldValue newValue
                    (true,  { oldCell with Value = Some Contradiction })
            | _ ->
                if PropagatorNetwork.Equalish (Some oldValue) (Some newValue)
                then (false, oldCell)
                else 
                  // raise (ContradictionException("Values are contradictory", oldValue, newValue))
                  printfn "LOG ERROR: %s %A %A" "Values are contradictory" oldValue newValue
                  (true, { oldCell with Value = Some Contradiction })

        // If old is None, new is strictly better
        // If new is None, old is strictly better
        // If both are populated, need to merge values (if possible)
        match (oldCell.Value, newCell.Value) with
        | Some(Contradiction), _ -> (false, { oldCell with Value = Some Contradiction })
        | _, Some(Contradiction) -> (false, { oldCell with Value = Some Contradiction })
        | None, Some(_) -> (true, newCell)
        | Some(_), None -> (false, oldCell)
        | Some(oldCell'), Some(newCell') -> mergeCells' oldCell' newCell'
        | None, None -> (false, oldCell)


    /// Are the cells equal (use id and underlying value to determine)
    static member private CellsEqual (a: CellDetail) (b: CellDetail) =
        if a.Id <> b.Id then false
        else if not (PropagatorNetwork.Equalish a.Value b.Value) then false // a.Value <> b.Value then false
        else true

    /// Are all the values in a list compatible?
    /// In most cases, they must all be the same or non-info
    /// In more advanced types (like ranges) overlapping data is compatible
    static member private IsCompatibleValues(valueList: DataType list) =
        if List.isEmpty valueList then
            true
        else
            let dt = (List.head valueList)
            match dt with
            | I(_) -> PropagatorNetwork.IsListAllIdentical valueList
            | F(_) -> PropagatorNetwork.IsListAllIdentical valueList
            | M(_) -> PropagatorNetwork.IsListAllIdentical valueList
            | B(_) -> PropagatorNetwork.IsListAllIdentical valueList
            | V(_) -> PropagatorNetwork.IsListAllIdentical valueList
            | Contradiction -> false

    //////////
    // Members

    /// Init propagator network
    member private __.Init() = ()

    /// Get the specified cell object from the network
    member private __.GetCellDetail(cell: Cell) =
        if cells.ContainsKey cell.Id then Some(cells.Item(cell.Id)) else None

    /// Extract the CellValue for the specified cell
    member private __.GetCellValue(cell: Cell): DataType option =
        if cells.ContainsKey cell.Id then (cells.Item(cell.Id)).Value else None

    /// Update a cell value manually
    member private __.UpdateCell (v: Cell) (newV: CellValue) =
        let (v2Found, v2) = cells.TryGetValue(v.Id)
        if not v2Found then
            // NOTE: This should never happen
            raise (UpdateException(sprintf "Cell %A not found." v.Id))

        if v2.IsConstant then raise (ConstantException(v2.ToString(), newV.ToString()))

        let updatedValue = newV

        let v' = { v2 with Value = updatedValue }

        let (propagatable, mergedCell) =
            if cells.ContainsKey(v'.Id) then
                // update
                let (found, currentCell) = cells.TryGetValue(v'.Id)
                let (propagatable, mergedCell) = PropagatorNetwork.MergeCells currentCell v'

                let propagatable =
                    if found then
                        let success = cells.TryUpdate(v'.Id, mergedCell, currentCell)
                        // printfn "success: %A" success
                        if mergedCell.Value.IsSome && mergedCell.Value.Value = Contradiction
                        then false
                        else not (PropagatorNetwork.CellsEqual currentCell mergedCell)
                    else
                        false

                (propagatable, mergedCell)
            else
                // add
                let success = cells.TryAdd(v'.Id, v')
                if success then (true, v') else (false, v')

        let resultCell = { Cell.Id = v'.Id }

        if propagatable then
            // Schedule impacted propagators
            propagators
            |> List.filter (fun x -> x.Operand1 = resultCell || x.Operand2 = resultCell || x.Operand3 = resultCell)
            |> List.iter __.SchedulePropagator

        resultCell

    /// Schedule a specific propagator function
    /// This applies for each provenance value impacted
    member private __.SchedulePropagatorFunction (propagtorFunction: PropagatorFcn) (operand1: Cell) (operand2: Cell)
           (operand3: Cell) =
        let operand3Cell: CellDetail option = __.GetCellDetail operand3
        if operand3Cell.IsSome && operand3Cell.Value.IsConstant then
            // operand3 is a constant, it should not be updated, don't do anything
            // NOTE: Skip for now. There may be an argument to allow this to flow through, in case it shows a data inconsistency
            // DEBUG: printfn "Skipping schedule due to constant: %A" operand3Cell
            ()
        else
            let f =
                fun x ->
                    // printfn "running a propagator..."
                    let operand1Cell: CellDetail option = __.GetCellDetail operand1
                    let operand2Cell: CellDetail option = __.GetCellDetail operand2

                    if operand1Cell.IsSome && operand2Cell.IsSome then
                        // Both cells are found
                        if operand1Cell.Value.Value.IsSome && operand2Cell.Value.Value.IsSome then
                            // Both cells have values
                            let operand1CellValue = operand1Cell.Value.Value.Value
                            let operand2CellValue = operand2Cell.Value.Value.Value
                            let newValue = propagtorFunction operand1CellValue operand2CellValue

                            if newValue.IsSome then
                                __.UpdateCell operand3 newValue |> ignore
                                true
                            else
                                false
                        else
                            false
                    else
                        false

            let job =
                { Job.Id = Job.NewJobId()
                  Timestamp = DateTime.UnixEpoch
                  JobFunction = f }

            scheduler.EnQueue job

    /// Schedule a propagator for execution
    /// This means all impacted functions, as appropriate
    member private __.SchedulePropagator(propagator: Propagator) =
        let operand1Value: DataType option = __.GetCellValue propagator.Operand1
        let operand2Value: DataType option = __.GetCellValue propagator.Operand2
        let operand3Value: DataType option = __.GetCellValue propagator.Operand3

        if operand1Value.IsSome && operand2Value.IsSome then
            __.SchedulePropagatorFunction propagator.Operator.f1 propagator.Operand1 propagator.Operand2
                propagator.Operand3
        if operand1Value.IsSome && operand3Value.IsSome then
            __.SchedulePropagatorFunction propagator.Operator.f2 propagator.Operand1 propagator.Operand3
                propagator.Operand2
        if operand2Value.IsSome && operand3Value.IsSome then
            __.SchedulePropagatorFunction propagator.Operator.f3 propagator.Operand2 propagator.Operand3
                propagator.Operand1


    ///////////////////
    // Public interface

    /// Create a propagator from an operator and 3 operands
    static member public CreatePropagator (operator: PropagatorOperator) (operand1: Cell) (operand2: Cell)
                  (operand3: Cell): Propagator =
        { Id = Propagator.AddPropagatorId()
          Operator = operator
          Operand1 = operand1
          Operand2 = operand2
          Operand3 = operand3 }

    /// Add a cell
    member public __.AddCell(v: 'a option): Cell =
        let cellValue = PropagatorNetwork.CreateCellValue v
        let cell = PropagatorNetwork.CreateCell cellValue
        cells.GetOrAdd(cell.Id, cell) |> ignore
        { Cell.Id = cell.Id }

    /// Add a constant-value cell
    member public __.AddConstant(v: 'a): Cell =
        let cellValue = PropagatorNetwork.CreateCellValue(Some v)
        let cell = PropagatorNetwork.CreateConstantCell cellValue
        cells.GetOrAdd(cell.Id, cell) |> ignore
        { Cell.Id = cell.Id }

    /// Update cell value
    member public __.SetCell (cell: Cell) (v: 'a option): Cell =
        let v' =
            if v.IsSome then Some(PropagatorNetwork.ToDataType v.Value) else None
        __.UpdateCell cell v'

    /// Get the raw value for the specified cell
    member public __.GetCell(cell: Cell): DataType option = __.GetCellValue cell

    /// Get the raw int value for the specified cell
    member public __.GetInt(cell: Cell): int option =
        let v = __.GetCellValue cell
        match v with
        | Some(I(v')) -> Some v'
        | _ -> None

    /// Get the raw float value for the specified cell
    member public __.GetFloat(cell: Cell): float option =
        let v = __.GetCellValue cell
        match v with
        | Some(F(v')) -> Some v'
        | _ -> None

    /// Get the raw decimal value for the specified cell
    member public __.GetDecimal(cell: Cell): decimal option =
        let v = __.GetCellValue cell
        match v with
        | Some(M(v')) -> Some v'
        | _ -> None

    /// Get the raw bool value for the specified cell
    member public __.GetBool(cell: Cell): bool option =
        let v = __.GetCellValue cell
        match v with
        | Some(B(v')) -> Some v'
        | _ -> None

    /// Get the raw interval value for the specified cell
    member public __.GetInterval(cell: Cell): Interval option =
        let v = __.GetCellValue cell
        match v with
        | Some(V(v')) -> Some v'
        | _ -> None

    /// Add a new propagator to the network
    member public __.AddPropagator(propagator: Propagator) =
        propagators <- propagator :: propagators
        __.SchedulePropagator propagator

    static member public addPropagator (network: PropagatorNetwork) (propagator: Propagator): PropagatorNetwork =
        let n' = network
        n'.AddPropagator propagator
        n'

    /// Run queued up processing
    member public __.Run() =
        try
            scheduler.Run()
        with
        | ConstantException(a, b) -> printfn "ERROR: Attempt to update constant %s to %s" a b
        | ContradictionException(message, a, b) -> printfn "%s (%A, %A)" message a b
        | InvalidIntervalException(message, a, b) -> printfn "%s (%A, %A)" message a b

    /// Dump network cells (for debugging)
    member public __.DumpCells title =
        printfn "CELLS: %s" title
        cells.Values |> Seq.iter (fun v -> printfn "%i %A" v.Id v.Value)

    /// Dump network propagators (for debugging)
    member public __.DumpPropagators title =
        printfn "PROPAGATORS: %s" title
        propagators
        |> List.iter (fun v -> printfn "%i %A %d %d %d" v.Id v.Operator v.Operand1.Id v.Operand2.Id v.Operand3.Id)

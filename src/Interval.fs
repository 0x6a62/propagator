namespace Propagator

open System


/// Interval dataype
[<Struct>]
type Interval = { 
  Min: float option; 
  Max: float option }
  with 

  /// String representation of interval  
  override __.ToString() =
    let format = "0.#####"
    let mins = if __.Min.IsSome then __.Min.Value.ToString(format) else "?"
    let maxs = if __.Max.IsSome then __.Max.Value.ToString(format) else "?"
    sprintf "[%s .. %s]" mins maxs
  
  /// Interval addition (a + b)
  static member (+) (a: Interval, b: Interval) :Interval =
    let min =  
      if a.Min.IsSome && b.Min.IsSome then Some (a.Min.Value + b.Min.Value)
      else None

    let max =  
      if a.Max.IsSome && b.Max.IsSome then Some (a.Max.Value + b.Max.Value)
      else None

    { Min = min; Max = max }

  /// Interval subtraction (a - b)
  static member (-) (a: Interval, b: Interval) :Interval =
    let min =  
      if a.Min.IsSome && b.Max.IsSome then Some (a.Min.Value - b.Max.Value)
      else None

    let max =  
      if a.Max.IsSome && b.Min.IsSome then Some (a.Max.Value - b.Min.Value)
      else None

    { Min = min; Max = max }

  /// Interval multiplication (a * b)
  static member (*) (a: Interval, b: Interval) :Interval =
    let amin = if a.Min.IsSome then a.Min.Value else Double.NegativeInfinity
    let amax = if a.Max.IsSome then a.Max.Value else Double.PositiveInfinity
    let bmin = if b.Min.IsSome then b.Min.Value else Double.NegativeInfinity
    let bmax = if b.Max.IsSome then b.Max.Value else Double.PositiveInfinity

    let c = [
      amin * bmin
      amin * bmax
      amax * bmin
      amax * bmax ]
   
    let min = List.min c |> (fun x -> if Double.IsInfinity x then None else Some x)
    let max = List.max c |> (fun x -> if Double.IsInfinity x then None else Some x)

    { Min = min; Max = max } 

  /// Interval division (a / b)
  static member (/) (a: Interval, b: Interval) :Interval =
    let bmin =
      if b.Max.IsNone then None
      else if b.Max.IsSome && b.Max.Value = 0. then None 
      else Some(1. / b.Max.Value) 

    let bmax =
      if b.Min.IsNone then None
      else if b.Min.IsSome && b.Min.Value = 0. then None 
      else Some(1. / b.Min.Value) 

    let b' = { Min = bmin; Max = bmax }
    a * b'

  /// Interval raise-to-power (a ** b)
  static member Pow (a: Interval) (b: Interval) :Interval =
    let amin = if a.Min.IsSome then a.Min.Value else Double.NegativeInfinity
    let amax = if a.Max.IsSome then a.Max.Value else Double.PositiveInfinity
    let bmin = if b.Min.IsSome then b.Min.Value else Double.NegativeInfinity
    let bmax = if b.Max.IsSome then b.Max.Value else Double.PositiveInfinity

    let c = [
      Math.Pow(amin, bmin)
      Math.Pow(amin, bmax)
      Math.Pow(amax, bmin)
      Math.Pow(amax, bmax) ]
   
    let min = List.min c |> (fun x -> if Double.IsInfinity x then None else Some x)
    let max = List.max c |> (fun x -> if Double.IsInfinity x then None else Some x)

    { Min = min; Max = max } 

  /// Interval root (a ** (1/b))
  static member Root (a: Interval) (b: Interval) :Interval =
    let b' = { Interval.Min = if b.Min.IsSome && b.Min.Value <> 0. then Some (1. / b.Min.Value) else None
               Max = if b.Max.IsSome && b.Max.Value <> 0. then Some (1. / b.Max.Value) else None }

    Interval.Pow a b'

  /// Interval Log (log(a, b))
  static member Log (a: Interval) (b: Interval) :Interval =
    let amin = if a.Min.IsSome then a.Min.Value else Double.NegativeInfinity
    let amax = if a.Max.IsSome then a.Max.Value else Double.PositiveInfinity
    let bmin = if b.Min.IsSome then b.Min.Value else Double.NegativeInfinity
    let bmax = if b.Max.IsSome then b.Max.Value else Double.PositiveInfinity

    let c = [
      Math.Log(amin, bmin)
      Math.Log(amin, bmax)
      Math.Log(amax, bmin)
      Math.Log(amax, bmax) ]
   
    let min = List.min c |> (fun x -> if Double.IsInfinity x then None else Some x)
    let max = List.max c |> (fun x -> if Double.IsInfinity x then None else Some x)

    { Min = min; Max = max } 

  /// Intersection between two intervals
  static member Intersect (a: Interval) (b: Interval) :Interval option =
    let newMin = 
      if a.Min.IsSome && b.Min.IsSome then Some (max a.Min.Value b.Min.Value)
      else if a.Min.IsNone then b.Min
      else a.Min
    
    let newMax = 
      if a.Max.IsSome && b.Max.IsSome then Some (min a.Max.Value b.Max.Value)
      else if a.Max.IsNone then b.Max
      else a.Max

    if newMin.IsNone || newMax.IsNone || newMin.Value <= newMax.Value then Some { Interval.Min = newMin; Max = newMax } else None

  /// Determine if interval b is better than interval a (tighter range)
  static member IsBetter (a: Interval) (b: Interval) :bool =
    let minIsBetter =
      match (a.Min, b.Min) with
      | Some(a'), Some(b') ->  a' < b'
      | None, Some(_)  -> true
      | Some(_), None -> false
      | None, None -> false

    let maxIsBetter =
      match (a.Max, b.Max) with
      | Some(a'), Some(b') ->  a' > b'
      | None, Some(_)  -> true
      | Some(_), None -> false
      | None, None -> false

    minIsBetter && maxIsBetter

  /// Determine if interval b is equal or better than interval a (tighter range)
  static member IsEqualOrBetter (a: Interval) (b: Interval) :bool =
    let minIsBetter =
      match (a.Min, b.Min) with
      | Some(a'), Some(b') ->  a' >= b'
      | None, Some(_)  -> true
      | Some(_), None -> false
      | None, None -> false

    let maxIsBetter =
      match (a.Max, b.Max) with
      | Some(a'), Some(b') ->  a' >= b'
      | None, Some(_)  -> true
      | Some(_), None -> false
      | None, None -> false

    minIsBetter && maxIsBetter

namespace Propagator

/// Internal library methods
module Internal =
  open System

  /// Random generator
  let rand = Random()
  
  /// Epsilon precision (decimal places)
  [<Literal>]
  let EpsilonPrecision = 10
  
  /// Epsilon precision (as float)
  [<Literal>]
  let Epsilon = 0.0000000001

  /// Change precedence
  let inline ($) f g = f (g)

  /// Flip parameters
  let inline flip f a b = f b a 
  
  /// Are two float values equal enough? (within epsilon)
  let EqualishFloat (a: float) (b: float) =
    Math.Abs(a - b) < Epsilon

  /// Are two float values equal enough? (within epsilon)
  let EqualishFloatOption (a: float option) (b: float option) =
    if a.IsNone && b.IsNone then true
    else if a.IsNone <> b.IsNone then false
    else Math.Abs(a.Value - b.Value) < Epsilon

  /// Are two decimal values equal enough? (within epsilon)
  let EqualishDecimal (a: decimal) (b: decimal) =
    // TODO: dynamic epsilon
    Math.Abs(a - b) < decimal Epsilon

  /// Are two decimal values equal enough? (within epsilon)
  let EqualishDecimalOption (a: decimal option) (b: decimal option) =
    if a.IsNone && b.IsNone then true
    else if a.IsNone <> b.IsNone then false
    else Math.Abs(a.Value - b.Value) < decimal Epsilon

  /// Math.Pow wrapper a ** b
  let inline floatPow (a: float) (b: float) :float = Math.Pow(a, b)

  /// Math.Log wrapper log(a, b)
  let inline floatLog (a: float) (b: float) :float = Math.Log(a, b)

  /// Internal library types 
  module Types =
    /// Cell Id (internal use)
    type CellId = int
    
    /// Propagator Id (internal use)
    type PropagatorId = int

    /// Cell datatype
    type DataType =
      /// Integer
      | I of int
      /// Float
      | F of float
      /// Decimal
      | M of decimal 
      /// Boolean
      | B of bool
      /// Interval
      | V of Interval
      /// Data Contradiction
      | Contradiction

    /// Value (This is the actual value, within all wrappers)
    type CellValue = DataType option

    /// Cell (internal representation)
    [<Struct>]
    type CellDetail = {
      /// Cell Id (internal use)
      Id: CellId
      /// Cell's value
      Value: CellValue
      /// Cell Value is a constant
      IsConstant: bool } with
      
      /// Create cell id  
      static member NewCellId () :CellId = 
        rand.Next(Int32.MaxValue)

    /// Cell
    [<Struct>]
    type Cell = {
      /// Cell Id (internal)
      Id: CellId }

    /// Propagator function signature
    type PropagatorFcn = DataType -> DataType -> DataType option

    /// Propagator Operator
    [<Struct>]
    type PropagatorOperator = {
      /// Function for: a _ b = c
      f1: PropagatorFcn
      /// Function for: a _ c = b 
      f2: PropagatorFcn
      /// Function for: b _ c = a
      f3: PropagatorFcn }

    /// Propagator
    [<Struct>]
    type Propagator = {
      /// Propagator Id (internal)
      Id: PropagatorId
      /// Operator
      Operator: PropagatorOperator
      /// Cell 1
      Operand1: Cell
      /// Cell 2
      Operand2: Cell
      /// Cell 3
      Operand3: Cell } with

      /// Create propagator id
      static member AddPropagatorId () :PropagatorId = 
        rand.Next(Int32.MaxValue)

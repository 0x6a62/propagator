namespace Propagator

/// Propagator types
module Types =
  ////////////////
  /// Public types

  /// Cell
  type Cell = Internal.Types.Cell

  /// Cell value dataype 
  type DataType = Internal.Types.DataType

  /////////////
  // Exceptions

  /// Exception while assigning to datatype
  exception DataTypeException of string
  /// Exception while attempting to update value
  exception UpdateException of string
  /// Attempt to update constant value
  exception ConstantException of string * string
  /// Values are contradictory
  exception ContradictionException of string * DataType * DataType
  /// Invalid interval 
  exception InvalidIntervalException of string * DataType * DataType
  /// Functionality not currently supported
  exception NotSupportedException of string

﻿module TestsProgram

open Expecto

[<EntryPoint>]
let main argv =
  Tests.runTestsInAssemblyWithCLIArgs [] [||] // defaultConfig argv

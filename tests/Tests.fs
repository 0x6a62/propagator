module Tests

open System
open Expecto
open FsCheck

open Propagator
open Propagator.PropagatorOperators
open Propagator.Internal
open Propagator.Types

let config = { FsCheckConfig.defaultConfig with maxTest = 200 }

/////////////////////////
// Helper functions/types

/// Show cell detail (for debugging)
let showDetail (n: PropagatorNetwork) (cell1: Cell) (cell2: Cell) (cell3: Cell) resultValue targetValue =
  // n.DumpCells "showdetail failure"
  printfn "a: %A" (n.GetCell cell1)
  printfn "b: %A" (n.GetCell cell2)
  printfn "c: %A" (n.GetCell cell3)
  printfn "result: %A target: %A" resultValue targetValue

let makeInterval (noneLikelihood: float) :Interval =
  let a = if rand.NextDouble() < noneLikelihood then None else Some (rand.NextDouble()) 
  let b = if rand.NextDouble() < noneLikelihood then None else Some (rand.NextDouble()) 
  let (intervalMin, intervalMax) =
    if a.IsSome && b.IsSome then
      if a.Value < b.Value then (a, b) else (b, a)
    else (a, b)

  { Interval.Min = intervalMin; Interval.Max = intervalMax }

let inline intervalGenerator () =
  let noneLikelihood = 0.1
  if rand.NextDouble() < noneLikelihood then None else Some (makeInterval noneLikelihood)


///////////
// syntax 1


let testAddPropInt1 (a: int option) (b: int option) (c: int option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  adder cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' + b')
    | Some(a'), None, Some(b') -> Some(b' - a')
    | None, Some(a'), Some(b') -> Some(b' - a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInt cell1)
    | _, None, _ -> (n.GetInt cell2)
    | _, _, None -> (n.GetInt cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testSubPropInt1 (a: int option) (b: int option) (c: int option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  subtracter cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' - b')
    | Some(a'), None, Some(b') -> Some(a' - b')
    | None, Some(a'), Some(b') -> Some(b' + a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInt cell1)
    | _, None, _ -> (n.GetInt cell2)
    | _, _, None -> (n.GetInt cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testMultiplyPropInt1 (a: int option) (b: int option) (c: int option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  multiplier cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' * b')
    | Some(a'), None, Some(b') -> if a' <> 0 then Some(b' / a') else None
    | None, Some(a'), Some(b') -> if a' <> 0 then Some(b' / a') else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInt cell1)
    | _, None, _ -> (n.GetInt cell2)
    | _, _, None -> (n.GetInt cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testDividePropInt1 (a: int option) (b: int option) (c: int option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  divider cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> if b' <> 0 then Some(a' / b') else None
    | Some(a'), None, Some(b') -> if b' <> 0 then Some(a' / b') else None
    | None, Some(a'), Some(b') -> Some(b' * a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInt cell1)
    | _, None, _ -> (n.GetInt cell2)
    | _, _, None -> (n.GetInt cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result


let testAddPropFloat1 (a: float option) (b: float option) (c: float option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  adder cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' + b')
    | Some(a'), None, Some(b') -> Some(b' - a')
    | None, Some(a'), Some(b') -> Some(b' - a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetFloat cell1)
    | _, None, _ -> (n.GetFloat cell2)
    | _, _, None -> (n.GetFloat cell3)
    | _ -> None // ERROR

  // let result =  resultValue = targetValue
  let result = EqualishFloatOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testSubPropFloat1 (a: float option) (b: float option) (c: float option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  subtracter cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' - b')
    | Some(a'), None, Some(b') -> Some(a' - b')
    | None, Some(a'), Some(b') -> Some(b' + a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetFloat cell1)
    | _, None, _ -> (n.GetFloat cell2)
    | _, _, None -> (n.GetFloat cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishFloatOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testMultiplyPropFloat1 (a: float option) (b: float option) (c: float option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  multiplier cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' * b')
    | Some(a'), None, Some(b') -> if a' <> 0. then Some(b' / a') else None
    | None, Some(a'), Some(b') -> if a' <> 0. then Some(b' / a') else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetFloat cell1)
    | _, None, _ -> (n.GetFloat cell2)
    | _, _, None -> (n.GetFloat cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishFloatOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testDividePropFloat1 (a: float option) (b: float option) (c: float option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  divider cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> if b' <> 0. then Some(a' / b') else None
    | Some(a'), None, Some(b') -> if b' <> 0. then Some(a' / b') else None
    | None, Some(a'), Some(b') -> Some(b' * a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetFloat cell1)
    | _, None, _ -> (n.GetFloat cell2)
    | _, _, None -> (n.GetFloat cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishFloatOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testAddPropDecimal1 (a: decimal option) (b: decimal option) (c: decimal option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  adder cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' + b')
    | Some(a'), None, Some(b') -> Some(b' - a')
    | None, Some(a'), Some(b') -> Some(b' - a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetDecimal cell1)
    | _, None, _ -> (n.GetDecimal cell2)
    | _, _, None -> (n.GetDecimal cell3)
    | _ -> None // ERROR

  // let result =  resultValue = targetValue
  // let result = Network<_>.EqualishOption resultValue targetValue
  let result = EqualishDecimalOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testSubPropDecimal1 (a: decimal option) (b: decimal option) (c: decimal option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  subtracter cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' - b')
    | Some(a'), None, Some(b') -> Some(a' - b')
    | None, Some(a'), Some(b') -> Some(b' + a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetDecimal cell1)
    | _, None, _ -> (n.GetDecimal cell2)
    | _, _, None -> (n.GetDecimal cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishDecimalOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testMultiplyPropDecimal1 (a: decimal option) (b: decimal option) (c: decimal option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  multiplier cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' * b')
    | Some(a'), None, Some(b') -> if a' <> 0.m then Some(b' / a') else None
    | None, Some(a'), Some(b') -> if a' <> 0.m then Some(b' / a') else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetDecimal cell1)
    | _, None, _ -> (n.GetDecimal cell2)
    | _, _, None -> (n.GetDecimal cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishDecimalOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testDividePropDecimal1 (a: decimal option) (b: decimal option) (c: decimal option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  divider cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> if b' <> 0.m then Some(a' / b') else None
    | Some(a'), None, Some(b') -> if b' <> 0.m then Some(a' / b') else None
    | None, Some(a'), Some(b') -> Some(b' * a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetDecimal cell1)
    | _, None, _ -> (n.GetDecimal cell2)
    | _, _, None -> (n.GetDecimal cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishDecimalOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result


let testAndPropBool1 (a: bool option) (b: bool option) (c: bool option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  ander cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' && b')
    | Some(a'), None, Some(b') -> if b' then Some(true)
                                  else if a' then Some(false) 
                                  else None
    | None, Some(a'), Some(b') -> if b' then Some(true)
                                  else if a' then Some(false)
                                  else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetBool cell1)
    | _, None, _ -> (n.GetBool cell2)
    | _, _, None -> (n.GetBool cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testOrPropBool1 (a: bool option) (b: bool option) (c: bool option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  orer cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' || b')
    | Some(a'), None, Some(b') -> if b' then
                                    if a' then None else Some(true)
                                  else
                                    if a' then Some(false) else None
    | None, Some(a'), Some(b') -> if b' then
                                    if a' then None else Some(true)
                                  else
                                    if a' then Some(false) else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetBool cell1)
    | _, None, _ -> (n.GetBool cell2)
    | _, _, None -> (n.GetBool cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testXorPropBool1 (a: bool option) (b: bool option) (c: bool option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  xorer cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' <> b')
    | Some(a'), None, Some(b') -> if b' then Some (not a') else Some (a')
    | None, Some(a'), Some(b') -> if b' then Some (not a') else Some (a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetBool cell1)
    | _, None, _ -> (n.GetBool cell2)
    | _, _, None -> (n.GetBool cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testTemperatureConversion1 (c: float option) (f: float option) =
  let n = PropagatorNetwork()
  let cellc = n.AddCell c
  let cell95 = n.AddConstant 1.8
  let cellc95 = n.AddCell None
  let cell32 = n.AddConstant 32.
  let cellf = n.AddCell f

  // F = C × 1.8 + 32 
  multiplier cellc cell95 cellc95 |> n.AddPropagator
  adder cellc95 cell32 cellf |> n.AddPropagator

  n.Run()

  let matches =
    match (c, f) with
    | Some(c'), None -> let f' = (c'  * 1.8) + 32.
                        (n.GetFloat cellf) = (Some f')
    | None, Some(f') -> let c' = (f' - 32.) / 1.8
                        (n.GetFloat cellc) = (Some c')
    | _ -> false // ERROR

  matches

let testAddPropInterval1 (a: Interval option) (b: Interval option) (c: Interval option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  adder cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' + b')
    | Some(a'), None, Some(b') -> Some(b' - a')
    | None, Some(a'), Some(b') -> Some(b' - a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInterval cell1)
    | _, None, _ -> (n.GetInterval cell2)
    | _, _, None -> (n.GetInterval cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testSubtractPropInterval1 (a: Interval option) (b: Interval option) (c: Interval option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  subtracter cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' - b')
    | Some(a'), None, Some(b') -> Some(a' - b')
    | None, Some(a'), Some(b') -> Some(b' + a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInterval cell1)
    | _, None, _ -> (n.GetInterval cell2)
    | _, _, None -> (n.GetInterval cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testMultiplyPropInterval1 (a: Interval option) (b: Interval option) (c: Interval option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  multiplier cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' * b')
    | Some(a'), None, Some(b') -> Some(b' / a')
    | None, Some(a'), Some(b') -> Some(b' / a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInterval cell1)
    | _, None, _ -> (n.GetInterval cell2)
    | _, _, None -> (n.GetInterval cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testDividePropInterval1 (a: Interval option) (b: Interval option) (c: Interval option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  divider cell1 cell2 cell3 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' / b')
    | Some(a'), None, Some(b') -> Some(a' / b')
    | None, Some(a'), Some(b') -> Some(a' * b')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInterval cell1)
    | _, None, _ -> (n.GetInterval cell2)
    | _, _, None -> (n.GetInterval cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

///////////
// syntax 2

let testAddPropInt2 (a: int option) (b: int option) (c: int option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^+ cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' + b')
    | Some(a'), None, Some(b') -> Some(b' - a')
    | None, Some(a'), Some(b') -> Some(b' - a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInt cell1)
    | _, None, _ -> (n.GetInt cell2)
    | _, _, None -> (n.GetInt cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testSubPropInt2 (a: int option) (b: int option) (c: int option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^- cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' - b')
    | Some(a'), None, Some(b') -> Some(a' - b')
    | None, Some(a'), Some(b') -> Some(b' + a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInt cell1)
    | _, None, _ -> (n.GetInt cell2)
    | _, _, None -> (n.GetInt cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testMultiplyPropInt2 (a: int option) (b: int option) (c: int option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^* cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' * b')
    | Some(a'), None, Some(b') -> if a' <> 0 then Some(b' / a') else None
    | None, Some(a'), Some(b') -> if a' <> 0 then Some(b' / a') else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInt cell1)
    | _, None, _ -> (n.GetInt cell2)
    | _, _, None -> (n.GetInt cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testDividePropInt2 (a: int option) (b: int option) (c: int option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^/ cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> if b' <> 0 then Some(a' / b') else None
    | Some(a'), None, Some(b') -> if b' <> 0 then Some(a' / b') else None
    | None, Some(a'), Some(b') -> Some(b' * a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInt cell1)
    | _, None, _ -> (n.GetInt cell2)
    | _, _, None -> (n.GetInt cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result


let testAddPropFloat2 (a: float option) (b: float option) (c: float option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^+ cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' + b')
    | Some(a'), None, Some(b') -> Some(b' - a')
    | None, Some(a'), Some(b') -> Some(b' - a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetFloat cell1)
    | _, None, _ -> (n.GetFloat cell2)
    | _, _, None -> (n.GetFloat cell3)
    | _ -> None // ERROR

  // let result =  resultValue = targetValue
  let result = EqualishFloatOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testSubPropFloat2 (a: float option) (b: float option) (c: float option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^- cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' - b')
    | Some(a'), None, Some(b') -> Some(a' - b')
    | None, Some(a'), Some(b') -> Some(b' + a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetFloat cell1)
    | _, None, _ -> (n.GetFloat cell2)
    | _, _, None -> (n.GetFloat cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishFloatOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testMultiplyPropFloat2 (a: float option) (b: float option) (c: float option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^* cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' * b')
    | Some(a'), None, Some(b') -> if a' <> 0. then Some(b' / a') else None
    | None, Some(a'), Some(b') -> if a' <> 0. then Some(b' / a') else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetFloat cell1)
    | _, None, _ -> (n.GetFloat cell2)
    | _, _, None -> (n.GetFloat cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishFloatOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testDividePropFloat2 (a: float option) (b: float option) (c: float option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^/ cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> if b' <> 0. then Some(a' / b') else None
    | Some(a'), None, Some(b') -> if b' <> 0. then Some(a' / b') else None
    | None, Some(a'), Some(b') -> Some(b' * a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetFloat cell1)
    | _, None, _ -> (n.GetFloat cell2)
    | _, _, None -> (n.GetFloat cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishFloatOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testAddPropDecimal2 (a: decimal option) (b: decimal option) (c: decimal option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^+ cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' + b')
    | Some(a'), None, Some(b') -> Some(b' - a')
    | None, Some(a'), Some(b') -> Some(b' - a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetDecimal cell1)
    | _, None, _ -> (n.GetDecimal cell2)
    | _, _, None -> (n.GetDecimal cell3)
    | _ -> None // ERROR

  // let result =  resultValue = targetValue
  // let result = Network<_>.EqualishOption resultValue targetValue
  let result = EqualishDecimalOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testSubPropDecimal2 (a: decimal option) (b: decimal option) (c: decimal option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^- cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' - b')
    | Some(a'), None, Some(b') -> Some(a' - b')
    | None, Some(a'), Some(b') -> Some(b' + a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetDecimal cell1)
    | _, None, _ -> (n.GetDecimal cell2)
    | _, _, None -> (n.GetDecimal cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishDecimalOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testMultiplyPropDecimal2 (a: decimal option) (b: decimal option) (c: decimal option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^* cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' * b')
    | Some(a'), None, Some(b') -> if a' <> 0.m then Some(b' / a') else None
    | None, Some(a'), Some(b') -> if a' <> 0.m then Some(b' / a') else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetDecimal cell1)
    | _, None, _ -> (n.GetDecimal cell2)
    | _, _, None -> (n.GetDecimal cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishDecimalOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testDividePropDecimal2 (a: decimal option) (b: decimal option) (c: decimal option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^/ cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> if b' <> 0.m then Some(a' / b') else None
    | Some(a'), None, Some(b') -> if b' <> 0.m then Some(a' / b') else None
    | None, Some(a'), Some(b') -> Some(b' * a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetDecimal cell1)
    | _, None, _ -> (n.GetDecimal cell2)
    | _, _, None -> (n.GetDecimal cell3)
    | _ -> None // ERROR

  // let result = resultValue = targetValue
  let result = EqualishDecimalOption resultValue targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testAndPropBool2 (a: bool option) (b: bool option) (c: bool option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^& cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' && b')
    | Some(a'), None, Some(b') -> if b' then Some(true)
                                  else if a' then Some(false) 
                                  else None
    | None, Some(a'), Some(b') -> if b' then Some(true)
                                  else if a' then Some(false)
                                  else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetBool cell1)
    | _, None, _ -> (n.GetBool cell2)
    | _, _, None -> (n.GetBool cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testOrPropBool2 (a: bool option) (b: bool option) (c: bool option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^| cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' || b')
    | Some(a'), None, Some(b') -> if b' then
                                    if a' then None else Some(true)
                                  else
                                    if a' then Some(false) else None
    | None, Some(a'), Some(b') -> if b' then
                                    if a' then None else Some(true)
                                  else
                                    if a' then Some(false) else None
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetBool cell1)
    | _, None, _ -> (n.GetBool cell2)
    | _, _, None -> (n.GetBool cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testXorPropBool2 (a: bool option) (b: bool option) (c: bool option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^^ cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' <> b')
    | Some(a'), None, Some(b') -> if b' then Some (not a') else Some (a')
    | None, Some(a'), Some(b') -> if b' then Some (not a') else Some (a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetBool cell1)
    | _, None, _ -> (n.GetBool cell2)
    | _, _, None -> (n.GetBool cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testTemperatureConversion2 (c: float option) (f: float option) =
  let n = PropagatorNetwork()
  let cellc = n.AddCell c
  let cell95 = n.AddConstant 1.8
  let cellc95 = n.AddCell None
  let cell32 = n.AddConstant 32.
  let cellf = n.AddCell f

  // F = C × 1.8 + 32 
  cellc95 ^= cellc ^* cell95 |> n.AddPropagator
  cellf ^= cellc95 ^+ cell32 |> n.AddPropagator

  n.Run()

  let matches =
    match (c, f) with
    | Some(c'), None -> let f' = (c'  * 1.8) + 32.
                        (n.GetFloat cellf) = (Some f')
    | None, Some(f') -> let c' = (f' - 32.) / 1.8
                        (n.GetFloat cellc) = (Some c')
    | _ -> false // ERROR

  matches

let testAddPropInterval2 (a: Interval option) (b: Interval option) (c: Interval option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^+ cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' + b')
    | Some(a'), None, Some(b') -> Some(b' - a')
    | None, Some(a'), Some(b') -> Some(b' - a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInterval cell1)
    | _, None, _ -> (n.GetInterval cell2)
    | _, _, None -> (n.GetInterval cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testSubtractPropInterval2 (a: Interval option) (b: Interval option) (c: Interval option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^- cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' - b')
    | Some(a'), None, Some(b') -> Some(a' - b')
    | None, Some(a'), Some(b') -> Some(b' + a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInterval cell1)
    | _, None, _ -> (n.GetInterval cell2)
    | _, _, None -> (n.GetInterval cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testMultiplyPropInterval2 (a: Interval option) (b: Interval option) (c: Interval option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^* cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' * b')
    | Some(a'), None, Some(b') -> Some(b' / a')
    | None, Some(a'), Some(b') -> Some(b' / a')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInterval cell1)
    | _, None, _ -> (n.GetInterval cell2)
    | _, _, None -> (n.GetInterval cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result

let testDividePropInterval2 (a: Interval option) (b: Interval option) (c: Interval option) f =
  let n = PropagatorNetwork()
  let cell1 = n.AddCell a
  let cell2 = n.AddCell b
  let cell3 = n.AddCell c

  cell3 ^= cell1 ^/ cell2 |> n.AddPropagator

  n.Run()

  let targetValue =
    match (a, b, c) with
    | Some(a'), Some(b'), None -> Some(a' / b')
    | Some(a'), None, Some(b') -> Some(a' / b')
    | None, Some(a'), Some(b') -> Some(a' * b')
    | _ -> None // ERROR

  let resultValue =
    match (a,b,c) with
    | None, _, _ -> (n.GetInterval cell1)
    | _, None, _ -> (n.GetInterval cell2)
    | _, _, None -> (n.GetInterval cell3)
    | _ -> None // ERROR

  let result = resultValue = targetValue
  if not result then showDetail n cell1 cell2 cell3 resultValue targetValue
  result


/// Test internal functions
[<Tests>]
let testsInternal =

  testList "Propagator Library (Internal)"
    [
      testPropertyWithConfig config "min.int" <| fun (a: int) (b: int) -> min a b = min b a

      testPropertyWithConfig config "min.float" <| fun (a: NormalFloat) (b: NormalFloat) -> min a b = min b a

      testPropertyWithConfig config "min.decimal" <| fun (a: decimal) (b: decimal) -> min a b = min b a

      testPropertyWithConfig config "max.int" <| fun (a: int) (b: int) -> max a b = max b a

      testPropertyWithConfig config "max.float" <| fun (a: NormalFloat) (b: NormalFloat) -> max a b = max b a

      testPropertyWithConfig config "max.decimal" <| fun (a: decimal) (b: decimal) -> max a b = max b a
    ]


/// Test public interface functions (int)
[<Tests>]
let testsPublicInterfaceIntSyntax1 =

  testList "Propagator Library (Public interface - int syntax1)"
    [
      testPropertyWithConfig config "add (c=none) - int (1)" <| fun (a: int) (b: int) ->
        testAddPropInt1 (Some a) (Some b) None

      testPropertyWithConfig config "add (b=none)- int (1)" <| fun (a: int) (b: int) ->
        testAddPropInt1 (Some a) None (Some b)

      testPropertyWithConfig config "add (a=none) - int (1)" <| fun (a: int) (b: int) ->
        testAddPropInt1 None (Some a) (Some b)

      testPropertyWithConfig config "subtract (c=none) - int (1)" <| fun (a: int) (b: int) ->
        testSubPropInt1 (Some a) (Some b) None

      testPropertyWithConfig config "subtract (b=none)- int (1)" <| fun (a: int) (b: int) ->
        testSubPropInt1 (Some a) None (Some b)

      testPropertyWithConfig config "subtract (a=none) - int (1)" <| fun (a: int) (b: int) ->
        testSubPropInt1 None (Some a) (Some b)

      testPropertyWithConfig config "multiply (c=none) - int (1)" <| fun (a: int) (b: int) ->
        testMultiplyPropInt1 (Some a) (Some b) None

      testPropertyWithConfig config "multiply (b=none)- int (1)" <| fun (a: int) (b: int) ->
        testMultiplyPropInt1 (Some a) None (Some (a * b)) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "multiply (a=none) - int (1)" <| fun (a: int) (b: int) ->
        testMultiplyPropInt1 None (Some a) (Some (a * b)) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (c=none) - int (1)" <| fun (a: int) (b: int) ->
        testDividePropInt1 (Some (a * b)) (Some b) None

      testPropertyWithConfig config "divide (b=none)- int (1)" <| fun (a: int) (b: int) ->
        testDividePropInt1 (Some (a * b)) None (Some b) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (a=none) - int (1)" <| fun (a: int) (b: int) ->
        testDividePropInt1 None (Some a) (Some b) // Note: for int, need to ensure it is evenly divisible
    ]


/// Test public interface functions (float)
[<Tests>]
let testsPublicInterfaceFloatSyntax1 =

  testList "Propagator Library (Public interface - float syntax1)"
    [
      testPropertyWithConfig config "add (c=none) - float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testAddPropFloat1 (Some (float a)) (Some (float b)) None

      testPropertyWithConfig config "add (b=none)- float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testAddPropFloat1 (Some (float a)) None (Some (float b))

      testPropertyWithConfig config "add (a=none) - float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testAddPropFloat1 None (Some (float a)) (Some (float b))

      testPropertyWithConfig config "subtract (c=none) - float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testSubPropFloat1 (Some (float a)) (Some (float b)) None

      testPropertyWithConfig config "subtract (b=none)- float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testSubPropFloat1 (Some (float a)) None (Some (float b))

      testPropertyWithConfig config "subtract (a=none) - float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testSubPropFloat1 None (Some (float a)) (Some (float b))

      testPropertyWithConfig config "multiply (c=none) - float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testMultiplyPropFloat1 (Some (float a)) (Some (float b)) None

      testPropertyWithConfig config "multiply (b=none)- float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testMultiplyPropFloat1 (Some (float a)) None (Some (float (a) * float(b))) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "multiply (a=none) - float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testMultiplyPropFloat1 None (Some (float a)) (Some (float (a) * float(b))) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (c=none) - float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testDividePropFloat1 (Some ((float a) * (float b))) (Some (float b)) None

      testPropertyWithConfig config "divide (b=none)- float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testDividePropFloat1 (Some ((float a) * (float b))) None (Some (float b)) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (a=none) - float (1)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testDividePropFloat1 None (Some (float a)) (Some (float b)) // Note: for int, need to ensure it is evenly divisible
    ]


/// Test public interface functions (float)
[<Tests>]
let testsPublicInterfaceDecimalSyntax1 =

  testList "Propagator Library (Public interface - decimal syntax1)"
    [
      testPropertyWithConfig config "add (c=none) - decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testAddPropDecimal1 (Some a') (Some b') None

      testPropertyWithConfig config "add (b=none)- decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testAddPropDecimal1 (Some a') None (Some b')

      testPropertyWithConfig config "add (a=none) - decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testAddPropDecimal1 None (Some a') (Some b')

      testPropertyWithConfig config "subtract (c=none) - decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testSubPropDecimal1 (Some a') (Some b') None

      testPropertyWithConfig config "subtract (b=none)- decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testSubPropDecimal1 (Some a') None (Some b')

      testPropertyWithConfig config "subtract (a=none) - decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testSubPropDecimal1 None (Some a') (Some b')

      testPropertyWithConfig config "multiply (c=none) - decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testMultiplyPropDecimal1 (Some a') (Some b') None

      testPropertyWithConfig config "multiply (b=none)- decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testMultiplyPropDecimal1 (Some a') None (Some (a' * b')) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "multiply (a=none) - decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testMultiplyPropDecimal1 None (Some a') (Some (a' * b')) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (c=none) - decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testDividePropDecimal1 (Some (a' * b')) (Some b') None

      testPropertyWithConfig config "divide (b=none)- decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testDividePropDecimal1 (Some (a' * b')) None (Some b') // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (a=none) - decimal (1)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testDividePropDecimal1 None (Some a') (Some b') // Note: for int, need to ensure it is evenly divisible
    ]


/// Test public interface functions (bool)
[<Tests>]
let testsPublicInterfaceBoolSyntax1 =

  testList "Propagator Library (Public interface - bool syntax1)"
    [
      testPropertyWithConfig config "and (c=none) - bool (1)" <| fun (a: bool) (b: bool) ->
        testAndPropBool1 (Some a) (Some b) None

      testPropertyWithConfig config "and (b=none)- bool (1)" <| fun (a: bool) (b: bool) ->
        testAndPropBool1 (Some a) None (Some (a && b))

      testPropertyWithConfig config "and (a=none) - bool (1)" <| fun (a: bool) (b: bool) ->
        testAndPropBool1 None (Some a) (Some (a && b))

      testPropertyWithConfig config "or (c=none) - bool (1)" <| fun (a: bool) (b: bool) ->
        testOrPropBool1 (Some a) (Some b) None

      testPropertyWithConfig config "or (b=none)- bool (1)" <| fun (a: bool) (b: bool) ->
        testOrPropBool1 (Some a) None (Some (a || b))

      testPropertyWithConfig config "or (a=none) - bool (1)" <| fun (a: bool) (b: bool) ->
        testOrPropBool1 None (Some a) (Some (a || b))

      testPropertyWithConfig config "xor (c=none) - bool (1)" <| fun (a: bool) (b: bool) ->
        testXorPropBool1 (Some a) (Some b) None

      testPropertyWithConfig config "xor (b=none)- bool (1)" <| fun (a: bool) (b: bool) ->
        testXorPropBool1 (Some a) None (Some (a <> b))

      testPropertyWithConfig config "xor (a=none) - bool (1)" <| fun (a: bool) (b: bool) ->
        testXorPropBool1 None (Some a) (Some (a <> b))
    ]


/// Test public interface functions (interval)
[<Tests>]
let testsPublicInterfaceIntervalSyntax1 =

  testList "Propagator Library (Public interface - interval syntax1)"
    [
      testPropertyWithConfig config "add (c=none) - interval (1)" <| fun () ->
        testAddPropInterval1 (intervalGenerator()) (intervalGenerator()) None

      testPropertyWithConfig config "add (b=none) - interval (1)" <| fun () ->
        testAddPropInterval1 (intervalGenerator()) None (intervalGenerator())

      testPropertyWithConfig config "add (a=none) - interval (1)" <| fun () ->
        testAddPropInterval1 None (intervalGenerator()) (intervalGenerator())

      testPropertyWithConfig config "subtract (c=none) - interval (1)" <| fun () ->
        testSubtractPropInterval1 (intervalGenerator()) (intervalGenerator()) None

      testPropertyWithConfig config "subtract (b=none) - interval (1)" <| fun () ->
        testSubtractPropInterval1 (intervalGenerator()) None (intervalGenerator())

      testPropertyWithConfig config "subtract (a=none) - interval (1)" <| fun () ->
        testSubtractPropInterval1 None (intervalGenerator()) (intervalGenerator())

      testPropertyWithConfig config "multiply (c=none) - interval (1)" <| fun () ->
        testMultiplyPropInterval1 (intervalGenerator()) (intervalGenerator()) None

      testPropertyWithConfig config "multiply (b=none) - interval (1)" <| fun () ->
        testMultiplyPropInterval1 (intervalGenerator()) None (intervalGenerator())

      testPropertyWithConfig config "multiply (a=none) - interval (1)" <| fun () ->
        testMultiplyPropInterval1 None (intervalGenerator()) (intervalGenerator())

      testPropertyWithConfig config "divide (c=none) - interval (1)" <| fun () ->
        testDividePropInterval1 (intervalGenerator()) (intervalGenerator()) None

      testPropertyWithConfig config "divide (b=none) - interval (1)" <| fun () ->
        testDividePropInterval1 (intervalGenerator()) None (intervalGenerator())

      testPropertyWithConfig config "divide (a=none) - interval (1)" <| fun () ->
        testDividePropInterval1 None (intervalGenerator()) (intervalGenerator())
    ]


/// Test public interface functions (int)
[<Tests>]
let testsPublicInterfaceIntSyntax2 =

  testList "Propagator Library (Public interface - int syntax2)"
    [
      testPropertyWithConfig config "add (c=none) - int (2)" <| fun (a: int) (b: int) ->
        testAddPropInt2 (Some a) (Some b) None

      testPropertyWithConfig config "add (b=none)- int (2)" <| fun (a: int) (b: int) ->
        testAddPropInt2 (Some a) None (Some b)

      testPropertyWithConfig config "add (a=none) - int (2)" <| fun (a: int) (b: int) ->
        testAddPropInt2 None (Some a) (Some b)

      testPropertyWithConfig config "subtract (c=none) - int (2)" <| fun (a: int) (b: int) ->
        testSubPropInt2 (Some a) (Some b) None

      testPropertyWithConfig config "subtract (b=none)- int (2)" <| fun (a: int) (b: int) ->
        testSubPropInt2 (Some a) None (Some b)

      testPropertyWithConfig config "subtract (a=none) - int (2)" <| fun (a: int) (b: int) ->
        testSubPropInt2 None (Some a) (Some b)

      testPropertyWithConfig config "multiply (c=none) - int (2)" <| fun (a: int) (b: int) ->
        testMultiplyPropInt2 (Some a) (Some b) None

      testPropertyWithConfig config "multiply (b=none)- int (2)" <| fun (a: int) (b: int) ->
        testMultiplyPropInt2 (Some a) None (Some (a * b)) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "multiply (a=none) - int (2)" <| fun (a: int) (b: int) ->
        testMultiplyPropInt2 None (Some a) (Some (a * b)) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (c=none) - int (2)" <| fun (a: int) (b: int) ->
        testDividePropInt2 (Some (a * b)) (Some b) None

      testPropertyWithConfig config "divide (b=none)- int (2)" <| fun (a: int) (b: int) ->
        testDividePropInt2 (Some (a * b)) None (Some b) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (a=none) - int (2)" <| fun (a: int) (b: int) ->
        testDividePropInt2 None (Some a) (Some b) // Note: for int, need to ensure it is evenly divisible
    ]


/// Test public interface functions (float)
[<Tests>]
let testsPublicInterfaceFloatSyntax2 =

  testList "Propagator Library (Public interface - float syntax2)"
    [
      testPropertyWithConfig config "add (c=none) - float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testAddPropFloat2 (Some (float a)) (Some (float b)) None

      testPropertyWithConfig config "add (b=none)- float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testAddPropFloat2 (Some (float a)) None (Some (float b))

      testPropertyWithConfig config "add (a=none) - float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testAddPropFloat2 None (Some (float a)) (Some (float b))

      testPropertyWithConfig config "subtract (c=none) - float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testSubPropFloat2 (Some (float a)) (Some (float b)) None

      testPropertyWithConfig config "subtract (b=none)- float" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testSubPropFloat2 (Some (float a)) None (Some (float b))

      testPropertyWithConfig config "subtract (a=none) - float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testSubPropFloat2 None (Some (float a)) (Some (float b))

      testPropertyWithConfig config "multiply (c=none) - float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testMultiplyPropFloat2 (Some (float a)) (Some (float b)) None

      testPropertyWithConfig config "multiply (b=none)- float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testMultiplyPropFloat2 (Some (float a)) None (Some (float (a) * float(b))) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "multiply (a=none) - float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testMultiplyPropFloat2 None (Some (float a)) (Some (float (a) * float(b))) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (c=none) - float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testDividePropFloat2 (Some ((float a) * (float b))) (Some (float b)) None

      testPropertyWithConfig config "divide (b=none)- float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testDividePropFloat2 (Some ((float a) * (float b))) None (Some (float b)) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (a=none) - float (2)" <| fun (a: NormalFloat) (b: NormalFloat) ->
        testDividePropFloat2 None (Some (float a)) (Some (float b)) // Note: for int, need to ensure it is evenly divisible
    ]


[<Tests>]
let testsPublicInterface_DecimalSyntax2 =

  testList "Propagator Library (Public interface - decimal syntax2)"
    [
      ////////// 
      // decimal 
      testPropertyWithConfig config "add (c=none) - decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testAddPropDecimal2 (Some a') (Some b') None

      testPropertyWithConfig config "add (b=none)- decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testAddPropDecimal2 (Some a') None (Some b')

      testPropertyWithConfig config "add (a=none) - decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testAddPropDecimal2 None (Some a') (Some b')

      testPropertyWithConfig config "subtract (c=none) - decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testSubPropDecimal2 (Some a') (Some b') None

      testPropertyWithConfig config "subtract (b=none)- decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testSubPropDecimal2 (Some a') None (Some b')

      testPropertyWithConfig config "subtract (a=none) - decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testSubPropDecimal2 None (Some a') (Some b')

      testPropertyWithConfig config "multiply (c=none) - decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testMultiplyPropDecimal2 (Some a') (Some b') None

      testPropertyWithConfig config "multiply (b=none)- decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testMultiplyPropDecimal2 (Some a') None (Some (a' * b')) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "multiply (a=none) - decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testMultiplyPropDecimal2 None (Some a') (Some (a' * b')) // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (c=none) - decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testDividePropDecimal2 (Some (a' * b')) (Some b') None

      testPropertyWithConfig config "divide (b=none)- decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testDividePropDecimal2 (Some (a' * b')) None (Some b') // Note: for int, need to ensure it is evenly divisible

      testPropertyWithConfig config "divide (a=none) - decimal (2)" <| fun (a: Decimal) (b: Decimal) ->
        let (a', b') = (Decimal.Round(a, EpsilonPrecision), Decimal.Round(b, EpsilonPrecision))
        testDividePropDecimal2 None (Some a') (Some b') // Note: for int, need to ensure it is evenly divisible
    ]


/// Test public interface functions (bool)
[<Tests>]
let testsPublicInterfaceBoolSyntax2 =

  testList "Propagator Library (Public interface - bool syntax2)"
    [
      testPropertyWithConfig config "and (c=none) - bool (2)" <| fun (a: bool) (b: bool) ->
        testAndPropBool2 (Some a) (Some b) None

      testPropertyWithConfig config "and (b=none)- bool (2)" <| fun (a: bool) (b: bool) ->
        testAndPropBool2 (Some a) None (Some (a && b))

      testPropertyWithConfig config "and (a=none) - bool (2)" <| fun (a: bool) (b: bool) ->
        testAndPropBool2 None (Some a) (Some (a && b))

      testPropertyWithConfig config "or (c=none) - bool (2)" <| fun (a: bool) (b: bool) ->
        testOrPropBool2 (Some a) (Some b) None

      testPropertyWithConfig config "or (b=none)- bool (2)" <| fun (a: bool) (b: bool) ->
        testOrPropBool2 (Some a) None (Some (a || b))

      testPropertyWithConfig config "or (a=none) - bool (2)" <| fun (a: bool) (b: bool) ->
        testOrPropBool2 None (Some a) (Some (a || b))

      testPropertyWithConfig config "xor (c=none) - bool (2)" <| fun (a: bool) (b: bool) ->
        testXorPropBool2 (Some a) (Some b) None

      testPropertyWithConfig config "xor (b=none)- bool (2)" <| fun (a: bool) (b: bool) ->
        testXorPropBool2 (Some a) None (Some (a <> b))

      testPropertyWithConfig config "xor (a=none) - bool (2)" <| fun (a: bool) (b: bool) ->
        testXorPropBool2 None (Some a) (Some (a <> b))
    ]


/// Test public interface functions (interval)
[<Tests>]
let testsPublicInterfaceIntervalSyntax2 =

  testList "Propagator Library (Public interface - interval syntax2)"
    [
      testPropertyWithConfig config "add (c=none) - interval (2)" <| fun () ->
        testAddPropInterval2 (intervalGenerator()) (intervalGenerator()) None

      testPropertyWithConfig config "add (b=none) - interval (2)" <| fun () ->
        testAddPropInterval2 (intervalGenerator()) None (intervalGenerator())

      testPropertyWithConfig config "add (a=none) - interval (2)" <| fun () ->
        testAddPropInterval2 None (intervalGenerator()) (intervalGenerator())

      testPropertyWithConfig config "subtract (c=none) - interval (2)" <| fun () ->
        testSubtractPropInterval2 (intervalGenerator()) (intervalGenerator()) None

      testPropertyWithConfig config "subtract (b=none) - interval (2)" <| fun () ->
        testSubtractPropInterval2 (intervalGenerator()) None (intervalGenerator())

      testPropertyWithConfig config "subtract (a=none) - interval (2)" <| fun () ->
        testSubtractPropInterval2 None (intervalGenerator()) (intervalGenerator())

      testPropertyWithConfig config "multiply (c=none) - interval (2)" <| fun () ->
        testMultiplyPropInterval2 (intervalGenerator()) (intervalGenerator()) None

      testPropertyWithConfig config "multiply (b=none) - interval (2)" <| fun () ->
        testMultiplyPropInterval2 (intervalGenerator()) None (intervalGenerator())

      testPropertyWithConfig config "multiply (a=none) - interval (2)" <| fun () ->
        testMultiplyPropInterval2 None (intervalGenerator()) (intervalGenerator())

      testPropertyWithConfig config "divide (c=none) - interval (2)" <| fun () ->
        testDividePropInterval2 (intervalGenerator()) (intervalGenerator()) None

      testPropertyWithConfig config "divide (b=none) - interval (2)" <| fun () ->
        testDividePropInterval2 (intervalGenerator()) None (intervalGenerator())

      testPropertyWithConfig config "divide (a=none) - interval (2)" <| fun () ->
        testDividePropInterval2 None (intervalGenerator()) (intervalGenerator())
    ]


/// Test specific comparison results
[<Tests>]
let testsSpecificCasesSyntax1 =
  testList "Specific tests syntax1" 
    [
      testPropertyWithConfig config "xor - bool none/true/true (1)" <| fun () ->
        testXorPropBool1 None (Some true) (Some true)

      testPropertyWithConfig config "xor - bool none/true/false (1)" <| fun () ->
        testXorPropBool1 None (Some true) (Some false)

      testPropertyWithConfig config "xor - bool none/false/true (1)" <| fun () ->
        testXorPropBool1 None (Some false) (Some true)

      testPropertyWithConfig config "xor - bool none/false/false (1)" <| fun () ->
        testXorPropBool1 None (Some false) (Some false)

      testPropertyWithConfig config "temp c/f (1)" <| fun (a: NormalFloat) ->
        testTemperatureConversion1 (Some (float a)) None

      testPropertyWithConfig config "temp f/c (1)" <| fun (a: NormalFloat) ->
        testTemperatureConversion1 None (Some (float a))

      testPropertyWithConfig config "temp c/f 0c (1)" <| fun () ->
        testTemperatureConversion1 (Some (float 0.)) None

      testPropertyWithConfig config "temp c/f 100c (1)" <| fun () ->
        testTemperatureConversion1 (Some (float 100.)) None

      testPropertyWithConfig config "temp f/c 32f (1)" <| fun () ->
        testTemperatureConversion1 None (Some (float 32.))

      testPropertyWithConfig config "temp f/c 212f (1)" <| fun () ->
        testTemperatureConversion1 None (Some (float 212.))
    ]


/// Test specific comparison results
[<Tests>]
let testsSpecificCasesSyntax2 =
  testList "Specific tests syntax2" 
    [
      testPropertyWithConfig config "xor - bool none/true/true" <| fun () ->
        testXorPropBool2 None (Some true) (Some true)

      testPropertyWithConfig config "xor - bool none/true/false" <| fun () ->
        testXorPropBool2 None (Some true) (Some false)

      testPropertyWithConfig config "xor - bool none/false/true" <| fun () ->
        testXorPropBool2 None (Some false) (Some true)

      testPropertyWithConfig config "xor - bool none/false/false" <| fun () ->
        testXorPropBool2 None (Some false) (Some false)

      testPropertyWithConfig config "temp c/f (2)" <| fun (a: NormalFloat) ->
        testTemperatureConversion2 (Some (float a)) None

      testPropertyWithConfig config "temp f/c (2)" <| fun (a: NormalFloat) ->
        testTemperatureConversion2 None (Some (float a))

      testPropertyWithConfig config "temp c/f 0c (2)" <| fun () ->
        testTemperatureConversion2 (Some (float 0.)) None

      testPropertyWithConfig config "temp c/f 100c (2)" <| fun () ->
        testTemperatureConversion2 (Some (float 100.)) None

      testPropertyWithConfig config "temp f/c 32f (2)" <| fun () ->
        testTemperatureConversion2 None (Some (float 32.))

      testPropertyWithConfig config "temp f/c 212f (2)" <| fun () ->
        testTemperatureConversion2 None (Some (float 212.))
    ]


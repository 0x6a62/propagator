﻿open System
open Propagator
open Propagator.PropagatorOperators
open Propagator.Types

let rand = Random()

[<EntryPoint>]
let main argv =
  
  /////////////////////////
  // Temperature conversion
  printfn "### Temperature conversion"
  
  let temperatureConverter (c: float option) (f: float option) =
    let network = PropagatorNetwork()
    let cellc = network.AddCell c
    let cell18 = network.AddConstant 1.8
    let cellc18 = network.AddCell None
    let cell32 = network.AddConstant 32.
    let cellf = network.AddCell f
    
    // F = C × 1.8 + 32 
    multiplier cellc cell18 cellc18 |> network.AddPropagator
    adder cellc18 cell32 cellf |> network.AddPropagator

    network.Run()
    (network.GetFloat cellc, network.GetFloat cellf)

  match (temperatureConverter (Some 20.) None) with
  | Some(c), Some(f) -> printfn "%6.2fc = %6.2ff" c f
  | _ -> printfn "Conversion undetermined"

  match (temperatureConverter None (Some 80.)) with
  | Some(c), Some(f) -> printfn "%6.2fc = %6.2ff" c f
  | _ -> printfn "Conversion undetermined"


  //////////////////////////////////
  // Temperature conversion (Kelvin)
  printfn "### Temperature conversion (Kelvin)"
 
  let temperatureConverterCFK (c: float option) (f: float option) (k: float option) =
    let network = PropagatorNetwork()
    let cellc = network.AddCell c
    let cell18 = network.AddConstant 1.8
    let cellc18 = network.AddCell None
    let cell32 = network.AddConstant 32.
    let cellf = network.AddCell f
    let cellk = network.AddCell k
    let cell27315 = network.AddConstant 273.15

    // F = C × 1.8 + 32 
    multiplier cellc cell18 cellc18 |> network.AddPropagator
    adder cellc18 cell32 cellf |> network.AddPropagator
    // C = K - 273.15
    subtracter cellk cell27315 cellc |> network.AddPropagator

    network.Run()
    (network.GetFloat cellc, network.GetFloat cellf, network.GetFloat cellk)
  
  [ (Some 20., None, None)
    (None, Some 80., None)
    (None, None, Some 200.) ]
  |> List.iter(fun (c, f, k) ->
    match (temperatureConverterCFK c f k) with
    | Some(c), Some(f), Some(k) -> printfn "%6.2fc = %6.2ff = %6.2fk" c f k
    | _ -> printfn "Conversion undetermined")


  ////////////////////////////////////////////
  // Temperature conversion (alternate syntax)
  printfn "### Temperature conversion (alternate syntax)"

  let temperatureConverterSyntax2 (c: float option) (f: float option) =
    let network = PropagatorNetwork()
    let cellc = network.AddCell c
    let cell95 = network.AddConstant 1.8
    let cellc95 = network.AddCell None
    let cell32 = network.AddConstant 32.
    let cellf = network.AddCell f
    
    // F = C × 1.8 + 32 
    cellc95 ^= cellc ^* cell95 |> network.AddPropagator
    cellf ^= cellc95 ^+ cell32 |> network.AddPropagator

    network.Run()
    (network.GetFloat cellc, network.GetFloat cellf)

  match (temperatureConverterSyntax2 (Some 20.) None) with
  | Some(c), Some(f) -> printfn "%6.2fc = %6.2ff" c f
  | _ -> printfn "Conversion undetermined"

  match (temperatureConverterSyntax2 None (Some 80.)) with
  | Some(c), Some(f) -> printfn "%6.2fc = %6.2ff" c f
  | _ -> printfn "Conversion undetermined"


  //////////////////
  // Building Height
  printfn "### Building Height"

  /// Height - using fall duration and shadow length and floor count
  let buildingHeight (fallTime: Interval option) (buildingShadow: Interval option) (barometerHeight: Interval option) (barometerShadow: Interval option) (floors: Interval option) (floorHeight: Interval option) =
    let buildingHeight = None
    let network = PropagatorNetwork()

    // Height based on fall time
    // Fall duration: d = 0.5 * g * t^2
    let fallTime = network.AddCell fallTime
    let two = network.AddConstant { Interval.Min = Some 2.; Max = Some 2.}
    let t2 = network.AddCell None
    let g = network.AddConstant { Interval.Min = Some    9.80665; Max = Some 9.80665 }
    let gt2 = network.AddCell None
    let half = network.AddConstant { Interval.Min = Some 0.5; Max = Some 0.5 }
    let buildingHeight = network.AddCell buildingHeight
    
    // t2 ^= fallTime ^** two |> network.AddPropagator
    // gt2 ^= g ^* t2 |> network.AddPropagator
    // buildingHeight ^= half ^* gt2 |> network.AddPropagator
    power fallTime two t2 |> network.AddPropagator
    multiplier g t2 gt2 |> network.AddPropagator
    multiplier half gt2 buildingHeight |> network.AddPropagator

    // Shadow
    // Similar triangles Shadow Ratio = Height / Shadow
    let barometerHeight = network.AddCell barometerHeight
    let barometerShadow = network.AddCell barometerShadow
    let buildingShadow = network.AddCell buildingShadow
    let shadowRatio = network.AddCell None

    // shadowRatio ^= barometerHeight ^/ barometerShadow |> network.AddPropagator
    // shadowRatio ^= buildingHeight ^/ buildingShadow |> network.AddPropagator
    divider barometerHeight barometerShadow shadowRatio |> network.AddPropagator
    divider buildingHeight buildingShadow shadowRatio |> network.AddPropagator

    // Floors
    let floorCount = network.AddCell floors
    let floorHeight = network.AddCell floorHeight
    
    // buildingHeight ^= floorHeight ^* floorCount |> network.AddPropagator
    multiplier floorHeight floorCount buildingHeight |> network.AddPropagator

    network.Run()
    
    (network.GetInterval fallTime, network.GetInterval buildingHeight, network.GetInterval buildingShadow, network.GetInterval floorCount, network.GetInterval floorHeight)

  let interval (min: float option) (max: float option) =
    Some { Interval.Min = min; Max = max }

  let fallTime = interval (Some 3.5) (Some 4.5)
  let buildingShadow = interval (Some 170.0) (Some 171.0)
  let barometerHeight = interval (Some 0.12) (Some 0.13)
  let barometerShadow = interval (Some 0.24) (Some 0.25)
  let floors = interval (Some 30.) (Some 30.)
  let floorHeight = interval (Some 3.) (Some 3.5)
  // let floors = interval (Some 25.) (Some 25.)

  let (_, height', _, _, _) = buildingHeight fallTime None None None None None
  printfn "Only fall time...                Height (m): %O" height'

  let (_, height', _, _, _) = buildingHeight None buildingShadow barometerHeight barometerShadow None None 
  printfn "Only shadow...                   Height (m): %O" height'

  let (_, height', _, _, _) = buildingHeight None None None None floors floorHeight
  printfn "Only floors...                   Height (m): %O" height'

  let (_, height', _, _, _) = buildingHeight fallTime buildingShadow barometerHeight barometerShadow None None 
  printfn "Fall time and shadow...          Height (m): %O" height'

  let (_, height', _, _, _) = buildingHeight fallTime buildingShadow barometerHeight barometerShadow floors floorHeight
  printfn "Fall time, shadow, and floors... Height (m): %O" height'

  let (_, height', _, _, floorHeight') = buildingHeight fallTime buildingShadow barometerHeight barometerShadow floors None
  printfn "Building Height (m): %O" height'
  printfn "Floor Height (m)   : %O" floorHeight'


  ///////////////////
  // Sample Network 1
  printfn "### Sample Network 1"

  let network1 = PropagatorNetwork()
  let cellA = network1.AddCell (Some (rand.Next(100)))
  let cellB = network1.AddCell (Some (rand.Next(100)))
  let cellC = network1.AddCell None
 
  cellC ^= cellA ^+ cellB |> network1.AddPropagator

  let cellD = network1.AddCell (Some (rand.Next(100)))
  let cellE = network1.AddCell None
  let cellF = network1.AddCell (Some (rand.Next(100)))
  subtracter cellD cellE cellF |> network1.AddPropagator

  let cellG = network1.AddCell None
  let cellH = network1.AddCell (Some (rand.Next(100)))
  let cellI = network1.AddCell (Some (rand.Next(100)))
  cellI ^<- cellG ^/ cellH |> network1.AddPropagator

  network1.Run()

  printfn "%A + %A = %A"
    (network1.GetInt cellA)
    (network1.GetInt cellB)
    (network1.GetInt cellC)

  printfn "%A - %A = %A"
    (network1.GetInt cellD)
    (network1.GetInt cellE)
    (network1.GetInt cellF)

  printfn "%A / %A = %A"
    (network1.GetInt cellG)
    (network1.GetInt cellH)
    (network1.GetInt cellI)


  ///////////////////
  // Sample Network 2
  printfn "### Sample Network 2"

  let network2 = PropagatorNetwork()
  let cellN = network2.AddCell (Some (rand.Next(100)))
  let cellO = network2.AddCell (Some (rand.Next(100)))
  let cellP = network2.AddCell None
  let cellQ = network2.AddCell (Some (rand.Next(100)))
  let cellR = network2.AddCell None // (Some (rand.Next(100)))
  
  adder cellN cellO cellP |> network2.AddPropagator
  adder cellQ cellR cellP |> network2.AddPropagator

  network2.Run()

  printfn "n: %A" (network2.GetInt cellN)
  printfn "o: %A" (network2.GetInt cellO)
  printfn "p: %A" (network2.GetInt cellP)
  printfn "q: %A" (network2.GetInt cellQ)
  printfn "r: %A" (network2.GetInt cellR)

  0

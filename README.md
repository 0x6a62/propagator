# Propagator

F# Implementation of Propagator Networks.  The design is inspired by Alexey Radul and Gerald Jay Sussman Propagator Networks.  This is work is experimental.  References section for more detail.


# Usage 

See the included Sample project for a more detailed example.  Below is a light example.

```
dotnet add package Propagator
```

```
open Propagator
open Propagator.PropagatorOperators

let network1 = PropagatorNetwork()
let cellA = network1.AddCell (Some (rand.Next(100)))
let cellB = network1.AddCell (Some (rand.Next(100)))
let cellC = network1.AddCell None

cellC ^= cellA ^+ cellB |> network1.AddPropagator

let cellD = network1.AddCell (Some (rand.Next(100)))
let cellE = network1.AddCell None
let cellF = network1.AddCell (Some (rand.Next(100)))
subtracter cellD cellE cellF |> network1.AddPropagator

let cellG = network1.AddCell None
let cellH = network1.AddCell (Some (rand.Next(100)))
let cellI = network1.AddCell (Some (rand.Next(100)))
cellI ^<- cellG ^/ cellH |> network1.AddPropagator

network1.Run()

printfn "%A + %A = %A"
  (network1.GetInt cellA)
  (network1.GetInt cellB)
  (network1.GetInt cellC)

printfn "%A - %A = %A"
  (network1.GetInt cellD)
  (network1.GetInt cellE)
  (network1.GetInt cellF)

printfn "%A / %A = %A"
  (network1.GetInt cellG)
  (network1.GetInt cellH)
  (network1.GetInt cellI)
```


# Development

```
# Build
dotnet build ./src

# Run tests
cd tests && dotnet run

# Sample
cd sample && dotnet run

# Package
dotnet pack ./src
```


# References

[The Art of the Propagator](http://web.mit.edu/~axch/www/art.pdf)

[Revised Report on the Propagator Model](https://groups.csail.mit.edu/mac/users/gjs/propagators/)

[Propagation Networks: A Flexible and Expressive Substrate for Computation](http://web.mit.edu/~axch/www/phd-thesis.pdf)

[Data Provenance in Distributed Propagator Networks](http://dig.csail.mit.edu/2010/Papers/IPAW/ipaw2010.pdf)


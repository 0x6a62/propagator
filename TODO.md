# TODO

- investigate additional datatypes
- scheduler
  - graph-based
  - multi-threaded
- restructure module and interfaces
- run through profiler
- network serialize/save
- network deserialize/load
- add contradiction scanning when running scheduler (for debugging)
- build graphical representation
- add error logs into network for post-run review (for debugging. maybe)
- computation expression implementation
- better epsilon
- reimplement provenance
- investigate pipeline ergonomics
